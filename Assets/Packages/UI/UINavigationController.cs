﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINavigationController : UIController {
	public UIController defaultController;

	private List<UIController> stack = new List<UIController>();

	public UIController activeController {
		get { return stack.Count > 0 ? stack[stack.Count - 1] : null; }
	}

	void Awake() {
		if(defaultController != null) {
			stack.Add(defaultController);

			defaultController.show = true;
		}
	}

	public void Push(UIController controller) {
		if(stack.Count > 0)
			Hide(activeController);

		Show(controller);

		stack.Add(controller);
	}

	public void Pop(UIController controller) {
		if(controller == activeController) {
			Hide(controller);

			stack.Remove(controller);

			if(stack.Count > 0)
				Show(activeController);
		} else {
			stack.Remove(controller);
		}
	}

	private void Show(UIController controller)  {
		controller.ViewWillAppear();
		controller.Appear();
		controller.ViewDidAppear();
	}

	private void Hide(UIController controller)  {
		controller.ViewWillDisappear();
		controller.Disappear();
		controller.ViewDidDisappear();
	}
}
