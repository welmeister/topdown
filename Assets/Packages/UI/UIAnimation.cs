﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimation : MonoBehaviour {
	public bool isPlaying { get; protected set; }

	protected Action callback;

	public virtual void Play(Action callback = null) {
		isPlaying = true;

		this.callback = callback;
	}

	public virtual void Complete() {
		isPlaying = false;

		if(callback != null) {
			callback();

			callback = null;
		}
	}

	public virtual void Stop() {
		isPlaying = false;

		callback = null;
	}
}
