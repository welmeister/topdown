﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {
	public event Action<UIController> onAppear;
	public event Action<UIController> onDisappear;

	public UIController rootControler;

	public UIAnimation appearAnimation;
	public UIAnimation disappearAnimation;

	public virtual bool show { get; set; }

	public static T Find<T>() where T : UIController  {
		return FindObjectOfType<T>();
	}

	public virtual void ViewWillAppear() {
		if(onAppear != null)
			onAppear(this);
	}

	public virtual void ViewWillDisappear() {
		if(onDisappear != null)
			onDisappear(this);
	}

	public virtual void ViewDidAppear() {}
	public virtual void ViewDidDisappear() {}

	public virtual void Appear() {
		if(disappearAnimation != null && disappearAnimation.isPlaying)
			disappearAnimation.Stop();

		if(appearAnimation != null)
			appearAnimation.Play();

		show = true;
	}

	public virtual void Disappear() {
		if(appearAnimation != null && appearAnimation.isPlaying)
			appearAnimation.Stop();

		if(disappearAnimation != null) {
			disappearAnimation.Play(
				() => {
					show = false;
				}
			);
		} else {
			show = false;
		}
	}
}
