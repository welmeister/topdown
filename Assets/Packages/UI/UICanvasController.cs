﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvasController : UIController {
	public event Action<UICanvasController> onClosed;
	public event Action<UICanvasController> onShowed;

	public Canvas canvas;

	public override bool show {
		get { return canvas.enabled; }
		set { canvas.enabled = value; }
	}

	protected virtual bool handleEscapeButton {
		get { return false; }
	}

	public virtual void Show() {
		var stateController = rootControler as UINavigationController;

		if(stateController != null)
			stateController.Push(this);

		if(onShowed != null)
			onShowed(this);
	}

	public virtual void Close() {
		var stateController = rootControler as UINavigationController;

		if(stateController != null)
			stateController.Pop(this);

		if(onClosed != null)
			onClosed(this);
	}

	protected virtual void OnEscapeButton() {}

#if UNITY_ANDROID || UNITY_EDITOR
	protected bool isFocused;

	void Update() {
		if(show && handleEscapeButton) {
			if(Input.GetKeyDown(KeyCode.Escape)) {
				isFocused = true;
			}

			if(isFocused && Input.GetKeyUp(KeyCode.Escape)) {
				isFocused = false;

				OnEscapeButton();
			}
		} else {
			isFocused = false;
		}
	}
#endif
}
