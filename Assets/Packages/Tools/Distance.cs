using UnityEngine;
using System.Collections;

public class Distance {
	public static bool IsLow(Vector3 point0, Vector3 point1, float range) {
		return (point0 - point1).sqrMagnitude < range * range;
	}

	public static bool IsHigh(Vector3 point0, Vector3 point1, float range) {
		return (point0 - point1).sqrMagnitude >= range * range;
	}

	public static bool IsBetween(Vector3 point0, Vector3 point1, float min, float max) {
		return IsLow(point0, point1, max) && IsHigh(point0, point1, min);
	}
}
