using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneResources : MonoBehaviour {
	private Dictionary<string, Queue<GameObject>> resources = new Dictionary<string, Queue<GameObject>>();

	private static SceneResources sceneResourcesReference;

	private static SceneResources current {
		get {
			if(sceneResourcesReference == null) {
				var gameObject = new GameObject("SceneResources");

				sceneResourcesReference = gameObject.AddComponent<SceneResources>();
			}

			return sceneResourcesReference;
		}
	}

	public static GameObject Pop(GameObject item, Vector3 position, Quaternion rotation) {
		var key = item.name;
		var resources = current.resources;

		if(resources.ContainsKey(key)) {
			var pool = resources[key];

			if(pool.Count > 0) {
				var element = pool.Dequeue();

				var location = element.transform;

				location.position = position;
				location.rotation = rotation;

				return element;
			}
		}

		return InstantiateItem(item, position, rotation);
	}

	public static void Push(GameObject item) {
		var key = item.name;
		var resources = current.resources;

		if(resources.ContainsKey(key)) {
			var pool = resources[key];

			pool.Enqueue(item);
		}
		else {
			var pool = new Queue<GameObject>();

			pool.Enqueue(item);

			resources.Add(key, pool);
		}
	}

	public static GameObject InstantiateItem(GameObject item, Vector3 position, Quaternion rotation) {
		var element = (GameObject)Instantiate(item, position, rotation, current.transform);

		element.name = item.name;

		return element;
	}
}
