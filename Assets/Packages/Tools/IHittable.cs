﻿using UnityEngine;

public interface IHittable {
	bool alive { get; }
	Vector3 position { get; }
	bool Hit(float damage, int damagerId);
	void Repair(float rate);
}
