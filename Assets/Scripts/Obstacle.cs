﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, IHittable {
	public Effect explosionPrefab;

	private bool destroyed;

	public bool alive => !destroyed;

	public Vector3 position => transform.position;

	public bool Hit(float damage, int damagerId) {
		if(explosionPrefab != null)
			Instantiate(explosionPrefab, transform.position, Quaternion.identity, transform.parent);

		destroyed = true;
		Destroy(gameObject);

		return true;
	}

	public void Repair(float rate) {

	}
}
