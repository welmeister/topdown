﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameoverScreen : UICanvasController {
	public string winnerText;
	public string loserText;

	public event Action onContinue;

	public Button okButton;

	public Text gameoverLabel;
	public Text rankLabel;
	public Text killsLabel;
	public Text timeLabel;

	void Start() {
		okButton.onClick.AddListener(() => {
			if(onContinue != null)
				onContinue();
		});
	}

	public void Show(GameResult gameResult) {
		rankLabel.text = string.Format("Rank #{0}", gameResult.rank);
		killsLabel.text = string.Format("Kills: {0}", gameResult.kills);
		timeLabel.text = string.Format("Time: {0:0}:{1:00}", Mathf.Floor(gameResult.time / 60), Mathf.Floor(gameResult.time % 60));
		gameoverLabel.text = gameResult.state == GameResult.State.Completed ? winnerText : loserText;

		Show();
	}
}
