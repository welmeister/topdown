﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : UICanvasController {
	public Text playersLabel;
	public Text killsLabel;

	public TouchButton touchButton;

	private IMovable movable;

	public int players {
		set { playersLabel.text = value.ToString(); }
	}

	public int kills {
		set { killsLabel.text = value.ToString(); }
	}

	private void Start() {
		touchButton.onTouch += Rotate;
	}

	public void Show(GameProgress gameProgress, IMovable movable) {
		this.movable = movable;

		gameProgress.onChanged += OnProgressChanged;

		OnProgressChanged(gameProgress.rank, gameProgress.playerKills);

		Show();
	}

	private void Rotate(float value) {
		movable.Rotate(value);
	}

	private void OnProgressChanged(int playerRank, int playerKills) {
		players = playerRank;
		kills = playerKills;
	}
}
