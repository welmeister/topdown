﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class TouchButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
	public event Action<float> onTouch;
	public float touchLerpTime = .5f;

	private float screenCenterX;
	private float lastDeltaX;

	private float touchTimer;

	void Start() {
		screenCenterX = Screen.width / 2;
	}

	void Update() {
		if(lastDeltaX != 0f) {
			var touchValue = Mathf.Lerp(0f, lastDeltaX, touchTimer / touchLerpTime);

			if(onTouch != null)
				onTouch(touchValue);

			touchTimer += Time.deltaTime;
		}
	}

	public void OnPointerDown(PointerEventData eventData) {
		SetTouchValue(eventData.position.x);
	}

	public void OnPointerUp(PointerEventData eventData) {
		lastDeltaX = 0;
	}

	public void OnDrag(PointerEventData eventData) {
		SetTouchValue(eventData.position.x);
	}

	private void SetTouchValue(float posX) {
		var newDeltaX = posX - screenCenterX;
		var sameSign = newDeltaX > 0 && lastDeltaX > 0 || newDeltaX < 0 && lastDeltaX < 0;
		if(!sameSign) {
			lastDeltaX = Mathf.Clamp(newDeltaX, -1f, 1f);
			touchTimer = 0f;
		}
	}
}
