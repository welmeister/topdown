﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainScreen : UICanvasController {
	public event Action onStart;

	public Button startButton;

	void Start() {
		startButton.onClick.AddListener(() => {
			if(onStart != null)
				onStart();
		});
	}
}
