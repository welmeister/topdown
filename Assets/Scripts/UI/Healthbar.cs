﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour {
	public Image view;

	private UnitArmor armor;

	public void Setup(UnitArmor armor) {
		armor.onChanged += UpdateView;

		UpdateView(armor.healthRatio);
	}

	private void UpdateView(float rate) {
		view.fillAmount = rate;

		gameObject.SetActive(rate < 1f);
	}

	void Update() {
		transform.rotation = Quaternion.identity;
	}
}
