﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Touchpad : MonoBehaviour {
	public event Action<Vector2> onDrag;

	private Vector3 startPoint;

	void Update() {
		if(Input.GetMouseButtonDown(0)) {
			startPoint = Input.mousePosition;
		}

		if(Input.GetMouseButton(0)) {
			if(onDrag != null)
				onDrag((Input.mousePosition - startPoint).normalized);
		}
	}
}
