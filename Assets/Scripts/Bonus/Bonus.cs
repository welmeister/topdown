﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Bonus : MonoBehaviour {
	public event Action<Bonus> onRemoved;

	public Vector3 position {
		get { return transform.position; }
	}

	public abstract void Pickup(Unit unit);

	public void Remove() {
		if(onRemoved != null)
			onRemoved(this);

		Destroy(gameObject);
	}

	void OnTriggerEnter2D(Collider2D col) {
		var unit = col.GetComponent<Unit>();
		if(unit != null)
			Pickup(unit);
	}
}
