﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medkit : Bonus {
	public float rate = 1f;

	public override void Pickup(Unit unit) {
		unit.Repair(rate);

		Remove();
	}
}
