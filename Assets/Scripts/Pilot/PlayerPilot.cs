﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPilot : Pilot {
	public TargetFinder targetFinder;
	public float targetFindInterval = 1f;
	public float targetShootMinAngle = 30f;

	private float targetFindCooldown;
	private bool shooting;

	public override void Setup(IControlable controlable, GameController gameController) {
		base.Setup(controlable, gameController);

		targetFinder.Setup(controlable);
		targetFindCooldown = Time.time;
	}

	private void Update() {
		var horizontalMovement = Input.GetAxis("Horizontal");
		var verticalMovement = Input.GetAxis("Vertical");
		controlable.Rotate(horizontalMovement);
		controlable.Move(verticalMovement < 0f ? verticalMovement / 2f : verticalMovement);

		if(Time.time > targetFindCooldown) {
			var target = targetFinder.FindOptimalTarget();
			if(target != null) {
				var dir = (target.position - controlable.position).normalized;
				var angle = Vector3.Angle(dir, controlable.forward);
				shooting = angle < targetShootMinAngle;
			}
			else
				shooting = false;
			targetFindCooldown = Time.time + targetFindInterval;
		}

		if(shooting)
			controlable.UseWeapon();
	}
}
