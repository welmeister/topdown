﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiPilot : Pilot {
	public UnitBehaviour defaultBehaviour;

	private void Start() {
		defaultBehaviour.Setup(this);
	}

	private void Update() {
		defaultBehaviour.UpdateBehaviour(Time.deltaTime);
	}
}
