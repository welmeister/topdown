﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pilot : MonoBehaviour {
	public IControlable controlable { get; protected set; }
	public GameController gameController { get; protected set; }

	public virtual void Setup(IControlable controlable, GameController gameController) {
		this.controlable = controlable;
		this.gameController = gameController;
	}
}
