﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFinder : MonoBehaviour {
	public float targetFindDistance = 10f;
	public int maxTargets = 5;
	public LayerMask targetMask;

	private IMovable movable;
	private Collider2D[] targets;

	public void Setup(IMovable movable) {
		this.movable = movable;
		targets = new Collider2D[maxTargets];
	}

	public IHittable FindOptimalTarget() {
		var result = null as IHittable;

		var maxRatio = -1.0f;

		var count = Physics2D.OverlapCircleNonAlloc(movable.position, targetFindDistance, targets, targetMask);
		for(var i = 0; i < count; i++) {
			var target = targets[i].GetComponent<IHittable>();

			if(target != null && target.alive && !target.Equals(movable)) {
				var ratio = Vector3.Dot(movable.forward, (target.position - movable.position).normalized);

				if(ratio > maxRatio) {
					maxRatio = ratio;
					result = target;
				}
			}
		}

		return result;
	}

	private void OnDrawGizmosSelected() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(movable.position, targetFindDistance);
	}
}
