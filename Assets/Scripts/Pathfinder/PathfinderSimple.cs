﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfinderSimple : IPathfinder {
	private PathfindingGrid grid;

	public PathfinderSimple(GameController gameController) {
		grid = gameController.grid;
	}

	public Vector3 GetWayPoint(Vector3 point) {
		return grid.GetRandomPoint();
	}
}
