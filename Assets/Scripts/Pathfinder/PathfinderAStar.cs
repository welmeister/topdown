﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PathfinderAStar : IPathfinder {
	private PathfindingGrid grid;
	private int maxSize;

	private Queue<Vector3> path = new Queue<Vector3>();

	public PathfinderAStar(GameController gameController) {
		grid = gameController.grid;
		maxSize = grid.size.x * grid.size.y;
	}

	public Vector3 GetWayPoint(Vector3 point) {
		if(path.Count == 0) {
			// var sw = new Stopwatch();
			// sw.Start();
			path = new Queue<Vector3>(FindPath(point, grid.GetRandomPoint()));
			// sw.Stop();
			// UnityEngine.Debug.Log(sw.ElapsedMilliseconds);
		}

		return path.Dequeue();
	}

	private List<Vector3> FindPath(Vector3 startPos, Vector3 endPos) {
		var startNode = grid.GetNodeByWorldPoint(startPos);
		var targetNode = grid.GetNodeByWorldPoint(endPos);

		var openSet = new Heap<PathfindingNode>(maxSize);
		var closedSet = new HashSet<PathfindingNode>();
		openSet.Add(startNode);

		while(openSet.Count > 0) {
			var currentNode = openSet.RemoveFirst();
			closedSet.Add(currentNode);

			if(currentNode.Equals(targetNode))
				return RetracePath(startNode, targetNode);

			foreach(var neighbourNode in grid.GetNeighbours(currentNode)) {
				if(!neighbourNode.walkable || closedSet.Contains(neighbourNode))
					continue;

				var newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbourNode);
				if(newMovementCostToNeighbour < neighbourNode.gCost || !openSet.Contains(neighbourNode)) {
					neighbourNode.gCost = newMovementCostToNeighbour;
					neighbourNode.hCost = GetDistance(neighbourNode, targetNode);
					neighbourNode.parent = currentNode;

					if(!openSet.Contains(neighbourNode))
						openSet.Add(neighbourNode);
				}
			}
		}

		return new List<Vector3>();
	}

	private List<Vector3> RetracePath(PathfindingNode startNode, PathfindingNode endNode) {
		var path = new List<Vector3>();
		var currentNode = endNode;

		while(currentNode != startNode) {
			path.Add(currentNode.worldPosition);
			currentNode = currentNode.parent;
		}

		path.Reverse();

		return path;
	}

	private int GetDistance(PathfindingNode nodeA, PathfindingNode nodeB) {
		var distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		var distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

		if(distX > distY)
			return 14 * distY + 10 * (distX - distY);
		else
			return 14 * distX + 10 * (distY - distX);
	}
}
