﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPathfinder {
	Vector3 GetWayPoint(Vector3 point);
}
