﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitTracks : MonoBehaviour {
	public float spawnDistance;
	public Effect prefab;

	private Vector3 lastPosition;

	private void Start() {
		lastPosition = transform.position;
	}

	private void Update() {
		if(Distance.IsHigh(lastPosition, transform.position, spawnDistance)) {
			lastPosition = transform.position;
			prefab.Instantiate<Effect>(transform.position, transform.rotation);
		}
	}
}
