﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Unit : MonoBehaviour, IHittable, IControlable {
	public event Action<Unit, int> onDie;
	// public event Action<float> onArmorChanged;

	public UnitConfig config;
	public Transform movementTransform;

	private IUnitMovement movement;
	private UnitWeapon weapon;
	private UnitArmor armor;

	public int id { get; private set; }
	public int team { get; private set; }
	public bool alive => armor && armor.alive;

	public Vector3 position => transform.position;
	public Vector3 forward => movementTransform.up;
	public Vector3 right => movementTransform.right;
	public float maxSpeed => movement.movementSpeed;

	private void FixedUpdate() {
		movement.UpdateMovement(Time.deltaTime);
	}

	public void Setup(int id, int team) {
		this.id = id;
		name = string.Format("Player{0}", id);

		movement = config.movementConfig.CreateMovement(movementTransform);

		weapon = GetComponent<UnitWeapon>();
		if(weapon)
			weapon.Setup(id, config.weaponConfig);

		armor = GetComponent<UnitArmor>();
		if(armor) {
			// armor.onChanged += ArmorChanged;
			armor.Setup(config.health);
		}
	}

	public bool Hit(float damage, int damagerId) {
		if(damagerId != id && armor) {
			armor.ApplyDamage(damage);

			if(!armor.alive) {
				armor.Explosion();

				onDie?.Invoke(this, damagerId);
				Remove();
			}
			else
				armor.Hit();

			return true;
		}

		return false;
	}

	public void Repair(float rate) {
		if(armor)
			armor.Restore(rate);
	}

	public void Move(float value) {
		movement.Move(value);
	}

	public void Rotate(Vector3 direction) {
		movement.Rotate(direction);
	}

	public void Rotate(float value) {
		Rotate(movementTransform.right * value);
	}

	public void UseWeapon() {
		if(weapon)
			weapon.Use();
	}

	public void Remove() {
		// if(armor)
		// 	armor.onChanged -= ArmorChanged;

		Destroy(gameObject);
	}

	// private void ArmorChanged(float rate) {
	// 	onArmorChanged?.Invoke(rate);
	// }
}
