﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMovementNone : IUnitMovement {
	public float movementSpeed => 0f;

	public void Setup(Transform transform, float movementSpeed, float rotationSpeed) {

	}

	public void Move(float movement) {

	}

	public void Rotate(Vector3 direction) {

	}

	public void UpdateMovement(float deltaTime) {

	}
}
