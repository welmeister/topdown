﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnitMovement {
	float movementSpeed { get; }
	void Setup(Transform transform, float movementSpeed, float rotationSpeed);
	void Move(float movement);
	void Rotate(Vector3 direction);
	void UpdateMovement(float deltaTime);
}
