﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMovementForward : IUnitMovement {
	private Transform transform;
	public float movementSpeed { get; private set; }
	private float rotationSpeed;

	private float movement;
	private Vector3 direction;

	public void Setup(Transform transform, float movementSpeed, float rotationSpeed) {
		this.transform = transform;
		this.movementSpeed = movementSpeed;
		this.rotationSpeed = rotationSpeed;
	}

	public void Move(float movement) {

	}

	public void Rotate(Vector3 direction) {
		this.direction = direction;
	}

	public void UpdateMovement(float deltaTime) {
		transform.position += transform.up * movementSpeed * deltaTime;

		if(direction != Vector3.zero) {
			var targetRotation = Quaternion.LookRotation(Vector3.forward, direction);

			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * deltaTime);
		}
	}
}
