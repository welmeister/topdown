﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitWeaponLauncher : UnitWeapon {
	private int firePointIndex;

	protected override void Shoot() {
		var firePoint = firePoints[firePointIndex++ % firePoints.Length];
		firePoint.gameObject.SetActive(false);

		var mount = mountPrefab.Instantiate<Mount>(firePoint.position, firePoint.rotation);
		mount.Setup(id, config.damage);
	}

	protected override void Reloaded() {
		base.Reloaded();

		for(int i = 0; i < firePoints.Length; i++)
			firePoints[i].gameObject.SetActive(true);
	}
}
