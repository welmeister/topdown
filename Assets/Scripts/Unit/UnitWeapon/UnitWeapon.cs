﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitWeapon : MonoBehaviour {
	public enum State { Ready, Cooldown, Reload }

	public Transform[] firePoints;
	public Mount mountPrefab;

	protected int id;
	protected UnitWeaponConfig config;

	private float cooldownTime;
	private int ammo;

	private State state;

	public void Setup(int id, UnitWeaponConfig weaponConfig) {
		this.id = id;
		config = weaponConfig;

		ammo = config.clip;

		Reloaded();
	}

	public void Use() {
		switch(state) {
			case State.Ready:
				state = State.Cooldown;
				cooldownTime = Time.time + (1 / config.fireRate);
				Shoot();

				if(config.clip > 0)
					ammo--;

				if(ammo == 0)
					Reload();
				break;
			case State.Cooldown:
				if(Time.time >= cooldownTime)
					state = State.Ready;
				break;
		}
	}

	protected virtual void Setup() { }
	protected virtual void Shoot() { }
	protected virtual void Reload() {
		state = State.Reload;
		Invoke("Reloaded", config.reloadTime);
	}
	protected virtual void Reloaded() {
		state = State.Ready;
		ammo = config.clip;
	}
}
