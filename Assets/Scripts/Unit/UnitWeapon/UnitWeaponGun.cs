﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitWeaponGun : UnitWeapon {
	public Effect shootEffectPrefab;
	public Effect muzzleEffectPrefab;
	public AudioClip audioClip;

	public float maxSpreadAngle = 10f;

	private int firePointIndex;

	protected override void Shoot() {
		var firePoint = firePoints[firePointIndex++ % firePoints.Length];
		var mountRotation = firePoint.rotation * Quaternion.AngleAxis(Random.Range(-maxSpreadAngle, maxSpreadAngle), Vector3.back);

		var mount = mountPrefab.Instantiate<Mount>(firePoint.position, mountRotation);
		mount.Setup(id, config.damage);

		if(shootEffectPrefab != null)
			shootEffectPrefab.Instantiate<Effect>(firePoint.position, firePoint.rotation);

		if(muzzleEffectPrefab != null)
			muzzleEffectPrefab.Instantiate<Effect>(firePoint.position, firePoint.rotation);
	}

}
