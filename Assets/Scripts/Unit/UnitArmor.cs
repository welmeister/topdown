﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitArmor : MonoBehaviour {
	public event Action<float> onChanged;

	public Healthbar healthbarPrefab;
	public Effect hitPrefab;
	public Effect explosionPrefab;
	public Effect scorchPrefab;

	private float maxHealth;
	private float health;
	private Healthbar healthbar;

	public bool alive => health > 0f;

	public float healthRatio => health / maxHealth;

	public void Setup(float maxHealth) {
		this.maxHealth = maxHealth;
		health = maxHealth;

		if(healthbarPrefab != null) {
			healthbar = Instantiate(healthbarPrefab, transform);
			healthbar.Setup(this);
		}
	}

	public void ApplyDamage(float value) {
		health = Mathf.Clamp(health - value, 0f, maxHealth);

		OnChanged();
	}

	public void Restore(float rate) {
		health = Mathf.Clamp(health + maxHealth * rate, 0f, maxHealth);

		OnChanged();
	}

	public void Hit() {
		if(hitPrefab)
			hitPrefab.Instantiate<Effect>(transform.position, Quaternion.identity);
	}

	public void Explosion() {
		if(explosionPrefab)
			explosionPrefab.Instantiate<Effect>(transform.position, Quaternion.identity);

		if(scorchPrefab)
			scorchPrefab.Instantiate<Effect>(transform.position, Quaternion.identity);
	}

	private void OnChanged() {
		if(onChanged != null)
			onChanged(healthRatio);
	}
}
