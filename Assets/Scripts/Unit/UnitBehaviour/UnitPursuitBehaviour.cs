﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPursuitBehaviour : UnitBehaviour {
	public TargetFinder targetFinder;
	public float targetFindInterval = 1f;
	public float targetFindCooldown = 10f;
	public float targetShootMinDistance = 8f;
	public float targetShootMinAngle = 30f;

	private float targetFindTime;
	private float targetCooldownTime;
	private IHittable target;

	public override void Setup(Pilot pilot) {
		base.Setup(pilot);

		targetFinder.Setup(pilot.controlable);
		targetFindTime = Time.time + Random.Range(0f, targetFindInterval);
		target = null;
	}

	public override bool UpdateBehaviour(float deltaTime) {
		if(Time.time >= targetFindTime) {
			targetFindTime = Time.time + targetFindInterval;
			var newTarget = targetFinder.FindOptimalTarget();
			if(newTarget != target) {
				target = newTarget;
				targetCooldownTime = Time.time + targetFindCooldown;
			}
		}

		if(Time.time < targetCooldownTime && target != null && target.alive) {
			var direction = (target.position - pilot.controlable.position).normalized;

			if(Distance.IsLow(target.position, pilot.controlable.position, targetShootMinDistance)) {
				pilot.controlable.Move(0f);

				var angle = Vector3.Angle(pilot.controlable.forward, direction);
				if(angle < targetShootMinAngle)
					pilot.controlable.UseWeapon();
			}
			else
				pilot.controlable.Move(1f);

			pilot.controlable.Rotate(direction);

			return true;
		}

		return false;
	}

	void OnDrawGizmosSelected() {
		if(pilot == null)
			return;

		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(pilot.controlable.position, targetShootMinDistance);

		if(target as Unit) {
			Gizmos.DrawLine(pilot.controlable.position, target.position);
			Gizmos.DrawWireSphere(target.position, 1f);
		}
	}
}
