﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitExploreBehaviour : UnitBehaviour {
	public enum PathfinderType { Simple, AStar }

	public PathfinderType pathfinderType;
	public float wayPointMinTravelTime = 5f;
	public float wayPointMinDistance = .5f;

	private IPathfinder pathfinder;

	private Vector3 wayPoint;
	private float waypointTimer;

	public override void Setup(Pilot pilot) {
		base.Setup(pilot);

		switch(pathfinderType) {
			case PathfinderType.Simple:
				pathfinder = new PathfinderSimple(pilot.gameController);
				break;
			case PathfinderType.AStar:
				pathfinder = new PathfinderAStar(pilot.gameController);
				break;
		}
	}

	public override bool UpdateBehaviour(float deltaTime) {
		if(fallbackBehaviour == null || !fallbackBehaviour.UpdateBehaviour(deltaTime)) {
			if(wayPoint != pilot.controlable.position)
				pilot.controlable.Rotate((wayPoint - pilot.controlable.position).normalized);

			pilot.controlable.Move(1f);

			if((waypointTimer -= deltaTime) <= 0f || Distance.IsLow(wayPoint, pilot.controlable.position, wayPointMinDistance)) {
				wayPoint = pathfinder.GetWayPoint(pilot.controlable.position);
				waypointTimer = wayPointMinTravelTime + (pilot.controlable.maxSpeed > 0 ? Vector3.Distance(wayPoint, pilot.controlable.position) / pilot.controlable.maxSpeed : 0f);
			}
		}

		return true;
	}

	private void OnDrawGizmosSelected() {
		if(pilot == null)
			return;

		Gizmos.color = Color.blue;
		Gizmos.DrawLine(pilot.controlable.position, wayPoint);
		Gizmos.DrawWireSphere(wayPoint, wayPointMinDistance);
	}
}
