﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitBehaviour : MonoBehaviour {
	public UnitBehaviour fallbackBehaviour;

	protected Pilot pilot { get; private set; }

	public virtual void Setup(Pilot pilot) {
		this.pilot = pilot;

		if(fallbackBehaviour != null)
			fallbackBehaviour.Setup(pilot);
	}

	public abstract bool UpdateBehaviour(float deltaTime);
}
