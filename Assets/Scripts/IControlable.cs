﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IControlable : IMovable {
	int id { get; }
	int team { get; }
	float maxSpeed { get; }

	void UseWeapon();
}
