﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {
	public float lifetime = 1f;

	private void OnEnable() {
		if(lifetime > 0)
			Remove(lifetime);
	}

	public T Instantiate<T>(Vector3 position, Quaternion rotation) where T : Effect {
		var element = SceneResources.Pop(gameObject, position, rotation);

		element.SetActive(true);

		return element.GetComponent<T>();
	}

	public void Remove(float time) {
		Invoke("Remove", time);
	}

	public virtual void Remove() {
		gameObject.SetActive(false);

		SceneResources.Push(gameObject);
	}
}
