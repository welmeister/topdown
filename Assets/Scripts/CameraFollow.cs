﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public Camera gameCamera;
	public float forward = 1f;
	public float smoothing;

	public AnimationCurve fovSpeedCurve;
	public float maxSpeed = 10f;

	private Vector3 velocity;
	private IControlable controlable;

	public void Follow(IControlable controlable) {
		this.controlable = controlable;

		if(controlable != null)
			gameCamera.orthographicSize = fovSpeedCurve.Evaluate(controlable.maxSpeed / maxSpeed);
	}

	private void FixedUpdate() {
		if(controlable != null)
			transform.position = Vector3.SmoothDamp(transform.position, controlable.position + (controlable.forward * forward), ref velocity, smoothing);
	}
}
