﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BonusSpawner : MonoBehaviour {
	public event Action<Bonus> onBonusRemoved;

	public Bonus prefab;

	public Bonus Spawn(Vector3 position, Transform parent) {
		var bonus = Instantiate(prefab, position, Quaternion.identity, parent);
		bonus.onRemoved += OnBonusRemoved;

		return bonus;
	}

	private void OnBonusRemoved(Bonus bonus) {
		if(onBonusRemoved != null)
			onBonusRemoved(bonus);
	}
}
