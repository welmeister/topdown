﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitSpawner : MonoBehaviour {
	public event Action<Unit, Pilot> onUnitSpawned;
	public event Action<Unit, int> onUnitRemoved;

	public Unit prefab;
	public Pilot pilotPrefab;

	public void Spawn(Vector3 position, Quaternion rotation, Transform parent) {
		var unit = Instantiate(prefab, position, rotation, parent);
		unit.onDie += OnUnitRemoved;

		var pilot = Instantiate(pilotPrefab, unit.transform);

		if(onUnitSpawned != null)
			onUnitSpawned(unit, pilot);
	}

	private void OnUnitRemoved(Unit unit, int id) {
		unit.onDie -= OnUnitRemoved;

		if(onUnitRemoved != null)
			onUnitRemoved(unit, id);
	}
}
