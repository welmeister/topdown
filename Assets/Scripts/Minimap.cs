﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour {
	public Camera renderCamera;

	public void StartRender() {
		renderCamera.enabled = true;
	}

	public void StopRender() {
		renderCamera.enabled = false;
	}
}
