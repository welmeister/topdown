﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mount : MonoBehaviour {
	public float distance = 20f;

	protected int id;
	protected float damage;

	public T Instantiate<T>(Vector3 position, Quaternion rotation) where T : Mount {
		var element = SceneResources.Pop(gameObject, position, rotation);

		element.SetActive(true);

		return element.GetComponent<T>();
	}

	public virtual void Remove() {
		gameObject.SetActive(false);

		SceneResources.Push(gameObject);
	}

	public virtual void Setup(int id, float damage) {
		this.id = id;
		this.damage = damage;
	}
}
