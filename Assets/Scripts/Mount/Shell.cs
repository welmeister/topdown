﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : Mount, IMovable {
	public float speed = 10f;

	private float totalDistance;

	public Vector3 forward => transform.up;
	public Vector3 position => transform.position;

	public override void Setup(int id, float damage) {
		base.Setup(id, damage);

		totalDistance = 0f;
	}

	public void Move(float value) {
		transform.position += forward * value;
	}

	public void Rotate(float value) { }

	public void Rotate(Vector3 value) { }

	private void Update() {
		if(totalDistance < distance) {
			var distanceDelta = speed * Time.deltaTime;

			totalDistance += distanceDelta;
			Move(distanceDelta);
		}
		else
			Remove();
	}

	private void OnTriggerEnter2D(Collider2D col) {
		var hittable = col.GetComponent<IHittable>();
		if(hittable != null && hittable.Hit(damage, id))
			Remove();
	}
}
