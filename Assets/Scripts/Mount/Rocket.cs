﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : Mount, IMovable {
	public TargetFinder targetFinder;
	public float speed = 10f;
	public float rotationSpeed = 90f;
	public Effect trailPrefab;

	private float totalDistance;
	private IHittable target;
	private Effect trail;

	public Vector3 forward => transform.up;
	public Vector3 position => transform.position;

	public override void Setup(int id, float damage) {
		base.Setup(id, damage);
		targetFinder.Setup(this);

		totalDistance = 0f;
		target = targetFinder.FindOptimalTarget();
		trail = trailPrefab.Instantiate<Effect>(transform.position, transform.rotation);
		trail.transform.SetParent(transform);
	}

	public override void Remove() {
		trail.transform.SetParent(null);
		trail.Remove(5f);

		base.Remove();
	}

	public void Move(float value) {
		transform.position += forward * value;
	}

	public void Rotate(float value) { }

	public void Rotate(Vector3 direction) {
		var targetRotation = Quaternion.LookRotation(Vector3.forward, direction);
		transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
	}

	private void Update() {
		if(totalDistance < distance) {
			var distanceDelta = speed * Time.deltaTime;

			totalDistance += distanceDelta;

			if(target != null && target.alive) {
				var direction = (target.position - transform.position).normalized;
				if(direction != Vector3.zero)
					Rotate(direction);
			}

			Move(distanceDelta);
		}
		else
			Remove();
	}

	private void OnTriggerEnter2D(Collider2D col) {
		var hittable = col.GetComponent<IHittable>();
		if(hittable != null && hittable.Hit(damage, id))
			Remove();
	}
}
