﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Mount {
	public LayerMask collisionMask;
	public float lifetime = .25f;
	public LineRenderer lineRenderer;
	public Color startColor, endColor;

	private Vector3 startPoint, endPoint;
	private float timer;

	public override void Setup(int id, float damage) {
		base.Setup(id, damage);

		timer = 0f;
		lineRenderer.startColor = lineRenderer.endColor = startColor;

		startPoint = transform.position;
		endPoint = startPoint + transform.up * distance;

		RaycastHit2D[] hits = new RaycastHit2D[2];

		var hitsCount = Physics2D.RaycastNonAlloc(startPoint, transform.up, hits, distance, collisionMask);
		if(hitsCount > 1) {
			var hittable = hits[1].collider.GetComponent<IHittable>();
			if(hittable != null && hittable.Hit(damage, id))
				endPoint = hits[1].point;
		}

		lineRenderer.SetPosition(0, startPoint);
		lineRenderer.SetPosition(1, endPoint);
	}

	private void Update() {
		if((timer += Time.deltaTime) < lifetime)
			lineRenderer.startColor = lineRenderer.endColor = Color.Lerp(startColor, endColor, timer / lifetime);
		else
			Remove();
	}
}
