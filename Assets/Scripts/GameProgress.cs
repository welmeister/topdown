﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameProgress {
	public event Action<int, int> onChanged;

	public bool completed;
	public bool successful;

	private int playerId;
	private Dictionary<int, int> kills;
	private float startGameTime;

	public int rank { get; private set; }

	public int playerKills {
		get { return GetKills(playerId); }
	}

	public float elapsedTime {
		get { return Time.time - startGameTime; }
	}

	public GameProgress() {
		playerId = -1;
		rank = 0;
		kills = new Dictionary<int, int>();
		startGameTime = Time.time;
	}

	public void SetPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public void ChangePlayersCount(int count) {
		rank = count;

		if(onChanged != null)
			onChanged(rank, playerKills);
	}

	public void AddKill(int id) {
		if(!kills.ContainsKey(id))
			kills.Add(id, 0);
		kills[id]++;
	}

	private int GetKills(int id) {
		return kills.ContainsKey(id) ? kills[id] : 0;
	}
}
