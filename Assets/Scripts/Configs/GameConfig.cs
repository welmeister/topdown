﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game", menuName = "Config/Game")]
public class GameConfig : ScriptableObject {
	public float gameTime = 180f;
	public int units = 50;
	public int teams = 1;
	public int bonuses = 10;
}
