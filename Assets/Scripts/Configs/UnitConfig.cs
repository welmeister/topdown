﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Unit", menuName = "Config/Unit")]
public class UnitConfig : ScriptableObject {
	public UnitMovementConfig movementConfig;
	public UnitWeaponConfig weaponConfig;
	public float health = 3f;
}

[System.Serializable]
public class UnitMovementConfig {
	public enum MovementType {
		None, Normal, ForwardOnly, RotateOnly
	}

	public MovementType movementType;
	public float rotationSpeed = 60f;
	public float movementSpeed = 5f;
	public float rotationOffsetRate;
	public float movementOffsetRate;

	public IUnitMovement CreateMovement(Transform transform) {
		var result = null as IUnitMovement;

		switch(movementType) {
			case MovementType.None:
				result = new UnitMovementNone();
				break;
			case MovementType.Normal:
				result = new UnitMovementNormal();
				break;
			case MovementType.ForwardOnly:
				result = new UnitMovementForward();
				break;
			case MovementType.RotateOnly:
				result = new UnitMovementRotate();
				break;
		}

		result.Setup(transform, GetMovementSpeed(), GetRotationSpeed());

		return result;
	}

	private float GetRotationSpeed() {
		var offset = rotationSpeed * rotationOffsetRate;
		return rotationSpeed + Random.Range(-offset, offset);
	}

	private float GetMovementSpeed() {
		var offset = movementSpeed * movementOffsetRate;
		return movementSpeed + Random.Range(-offset, offset);
	}
}

[System.Serializable]
public class UnitWeaponConfig {
	public float fireRate = 1f;
	public float reloadTime = 1f;
	public int clip = -1;
	public float damage = 1f;
}