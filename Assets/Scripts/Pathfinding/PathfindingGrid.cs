﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Tilemaps;

public class PathfindingGrid : MonoBehaviour {
	public Vector2Int size;
	public float scale = 1f;
	public Tilemap walkableLayer;

	private Vector2 worldSize;
	private PathfindingNode[,] nodes;

	private void Awake() {
		Generate();
	}

	private void Generate() {
		worldSize = new Vector2(size.x * scale, size.y * scale);
		nodes = new PathfindingNode[size.x, size.y];

		for(int x = 0; x < size.x; x++) {
			for(int y = 0; y < size.y; y++) {
				var pos = new Vector3Int(x - size.x / 2, y - size.y / 2, 0);
				var walkable = walkableLayer.HasTile(pos);
				var worldPos = walkableLayer.GetCellCenterWorld(pos);
				nodes[x, y] = new PathfindingNode(walkable, worldPos, x, y);
			}
		}
	}

	public Vector3 GetRandomPoint() {
		while(true) {
			var rndNode = nodes[Random.Range(0, size.x), Random.Range(0, size.y)];
			if(rndNode.walkable)
				return rndNode.worldPosition;
		}
	}

	public List<PathfindingNode> GetNeighbours(PathfindingNode node) {
		var result = new List<PathfindingNode>();

		for(int x = -1; x <= 1; x++) {
			for(int y = -1; y <= 1; y++) {
				if(x == 0 && y == 0)
					continue;

				var neighbourX = node.gridX + x;
				var neighbourY = node.gridY + y;
				if(InBounds(neighbourX, neighbourY))
					result.Add(nodes[neighbourX, neighbourY]);
			}
		}

		return result;
	}

	public PathfindingNode GetNodeByWorldPoint(Vector3 worldPoint) {
		var rateX = Mathf.Clamp01((worldPoint.x + worldSize.x / 2f - scale / 2f) / worldSize.x);
		var rateY = Mathf.Clamp01((worldPoint.y + worldSize.y / 2f - scale / 2f) / worldSize.y);

		var x = Mathf.RoundToInt(size.x * rateX);
		var y = Mathf.RoundToInt(size.y * rateY);

		return nodes[x, y];
	}

	private bool InBounds(int x, int y) {
		return x >= 0 && x < size.x && y >= 0 && y < size.y;
	}

	public List<PathfindingNode> path;
	private void OnDrawGizmosSelected() {
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube(Vector3.zero, new Vector3(size.x, size.y) * scale);

		if(nodes == null)
			return;

		foreach(var node in nodes) {
			Gizmos.color = node.walkable ? Color.green : Color.red;
			if(path != null && path.Contains(node))
				Gizmos.color = Color.black;
			Gizmos.DrawWireSphere(node.worldPosition, 1f);
		}
	}
}
