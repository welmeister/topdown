﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingNode : IHeapItem<PathfindingNode> {
	public bool walkable;
	public Vector3 worldPosition;
	public int gridX;
	public int gridY;

	public int gCost;
	public int hCost;
	public PathfindingNode parent;

	public int fCost => gCost + hCost;

	private int heapindex;
	public int HeapIndex {
		get => heapindex;
		set => heapindex = value;
	}

	public int CompareTo(PathfindingNode item) {
		var result = fCost.CompareTo(item.fCost);

		if(result == 0)
			result = hCost.CompareTo(item.hCost);

		return -result;
	}

	public PathfindingNode(bool walkable, Vector3 worldPosition, int gridX, int gridY) {
		this.walkable = walkable;
		this.worldPosition = worldPosition;
		this.gridX = gridX;
		this.gridY = gridY;
	}
}
