﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResult {
	public enum State {
		Quit, Completed, Failed
	}

	public State state;
	public int rank;
	public int kills;
	public float time;

	public GameResult(GameProgress gameProgress) {
		state = gameProgress.completed ? gameProgress.successful ? State.Completed : State.Failed : State.Quit;
		rank = gameProgress.rank;
		kills = gameProgress.playerKills;
		time = gameProgress.elapsedTime;
	}
}
