﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable {
	Vector3 position { get; }
	Vector3 forward { get; }

	void Move(float value);
	void Rotate(Vector3 direction);
	void Rotate(float value);
}
