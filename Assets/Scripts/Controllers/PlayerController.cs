﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public GameConfig gameConfig;

	public BattleRoyaleGameController gameController;
	public AudioController audioController;

	public MainScreen mainScreen;
	public GameScreen gameScreen;
	public GameoverScreen gameoverScreen;

	public float finishTime = 1f;

	void Start() {
		gameController.onStateChanged += (GameController.State state) => {
			switch(state) {
				case GameController.State.Playing:
					gameScreen.Show(gameController.gameProgress, gameController.movable);
					audioController.PlayGameMusic();
					break;
				case GameController.State.Gameover:
					gameScreen.Close();
					Invoke(gameController.gameProgress.successful ? "Win" : "Lose", finishTime);
					break;
				case GameController.State.Finished:
					break;
				default:
					break;
			}
		};

		mainScreen.onStart += () => {
			mainScreen.Close();
			gameController.StartGame();
			audioController.PlayStartSound();
		};

		gameoverScreen.onContinue += () => {
			gameoverScreen.Close();
			gameController.FinishGame();

			ShowMainScreen();
		};

		gameController.PrepareGame(gameConfig);
		ShowMainScreen();
	}

	private void ShowMainScreen() {
		mainScreen.Show();
		audioController.PlayMenuMusic();
	}

	private void Win() {
		gameoverScreen.Show(new GameResult(gameController.gameProgress));
		audioController.PlayWinSound();
	}

	private void Lose() {
		gameoverScreen.Show(new GameResult(gameController.gameProgress));
		audioController.PlayLoseSound();
	}
}
