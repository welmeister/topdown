﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class GameController : MonoBehaviour {
	public event Action<State> onStateChanged;

	public PathfindingGrid grid;

	public enum State {
		Idle, Playing, Gameover, Finished
	}

	private State state = State.Idle;

	public State gameState {
		get { return state; }
		protected set {
			if(state == value)
				return;
			state = value;

			if(onStateChanged != null)
				onStateChanged(state);
		}
	}

	public GameConfig gameConfig {
		get; private set;
	}

	public virtual void PrepareGame(GameConfig gameConfig) {
		this.gameConfig = gameConfig;
	}

	public virtual void StartGame() { }

	public virtual void FinishGame() { }

	public virtual Vector3 GetSpawnPoint() {
		return grid.GetRandomPoint();
	}

	public virtual Vector3 GetWayPoint() {
		return Vector3.zero;
	}
}
