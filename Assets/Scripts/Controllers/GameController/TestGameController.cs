﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGameController : GameController {
	public GameConfig config;
	public UnitsController unitsController;
	public BonusController bonusController;
	public GameObject environmentPrefab;

	private GameObject environment;

	private void Start() {
		PrepareGame(config);
	}

	public override void PrepareGame(GameConfig gameConfig) {
		base.PrepareGame(gameConfig);

		unitsController.Setup(this, gameConfig.units);
		// bonusController.Setup(this, gameConfig.bonuses);

		environment = Instantiate(environmentPrefab, transform);

		unitsController.Run(environment.transform);
		// bonusController.Run(environment.transform);
	}
}
