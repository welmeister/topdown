﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleRoyaleGameController : GameController {
	public CameraFollow cameraFollow;
	public Minimap minimap;

	public UnitsController unitsController;
	public BonusController bonusController;

	public GameObject environmentPrefab;

	private GameObject environment;

	public GameProgress gameProgress {
		get; private set;
	}

	public IMovable movable {
		get; protected set;
	}

	public override void PrepareGame(GameConfig gameConfig) {
		base.PrepareGame(gameConfig);

		unitsController.onPlayerSpawned += (Unit unit) => {
			gameProgress.SetPlayerId(unit.id);
			cameraFollow.Follow(unit);
			movable = unit;
		};

		unitsController.onPlayerDied += () => {
			Gameover();
		};

		unitsController.onUnitSpawned += (int count) => {
			if(count == gameConfig.units) {
				gameProgress.ChangePlayersCount(count);

				minimap.StartRender();
				gameState = State.Playing;
			}
		};

		unitsController.onUnitRemoved += (int count, int id) => {
			if(gameState == State.Playing) {
				gameProgress.AddKill(id);
				gameProgress.ChangePlayersCount(count);

				if(gameProgress.rank == 1) {
					gameProgress.successful = true;

					Gameover();
				}
			}
		};

		unitsController.Setup(this, gameConfig.units);
		bonusController.Setup(this, gameConfig.bonuses);
	}

	public override void StartGame() {
		gameProgress = new GameProgress();

		environment = Instantiate(environmentPrefab, transform);

		unitsController.Run(environment.transform);
		bonusController.Run(environment.transform);
	}

	public override void FinishGame() {
		bonusController.Stop();

		if(environment != null)
			Destroy(environment);

		gameState = State.Finished;
	}

	private void Gameover() {
		gameProgress.completed = true;

		minimap.StopRender();
		cameraFollow.Follow(null);

		gameState = State.Gameover;
	}

	public override Vector3 GetWayPoint() {
		var playerChance = 5f / unitsController.units.Count;
		if(Random.value <= playerChance)
			return GetPlayerPoint();

		var bonusChest = 2f / unitsController.units.Count;
		if(Random.value <= bonusChest)
			return GetRandomBonusPoint();

		return GetRandomUnitPoint();
	}

	public Vector3 GetPlayerPoint() {
		return movable as Unit ? movable.position : GetRandomUnitPoint();
	}

	public Vector3 GetRandomUnitPoint() {
		return unitsController.GetRandomUnitPoint();
	}

	public Vector3 GetRandomBonusPoint() {
		return bonusController.GetRandomBonusPoint();
	}
}
