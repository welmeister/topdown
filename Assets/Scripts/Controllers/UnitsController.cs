﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitsController : MonoBehaviour {
	public event Action<int> onUnitSpawned;
	public event Action<int, int> onUnitRemoved;
	public event Action<Unit> onPlayerSpawned;
	public event Action onPlayerDied;

	public UnitSpawner playerSpawner;
	public UnitSpawner[] enemySpawners;

	private GameController gameController;
	private int maxCount;

	public List<Unit> units {
		get; private set;
	}

	private void Awake() {
		playerSpawner.onUnitSpawned += (Unit unit, Pilot pilot) => {
			unit.Setup(units.Count, units.Count);
			pilot.Setup(unit, gameController);

			if(onPlayerSpawned != null)
				onPlayerSpawned(unit);

			AddUnit(unit);
		};
		playerSpawner.onUnitRemoved += (Unit unit, int id) => {
			if(onPlayerDied != null)
				onPlayerDied();

			RemoveUnit(unit, id);
		};

		for(int i = 0; i < enemySpawners.Length; i++) {
			enemySpawners[i].onUnitSpawned += (Unit unit, Pilot pilot) => {
				unit.Setup(units.Count, units.Count);
				pilot.Setup(unit, gameController);

				AddUnit(unit);
			};
			enemySpawners[i].onUnitRemoved += RemoveUnit;
		}
	}

	public void Setup(GameController gameController, int maxCount) {
		this.maxCount = maxCount;
		this.gameController = gameController;
	}

	public void Run(Transform container) {
		units = new List<Unit>();

		var enemySpawnerIndex = 0;
		for(int i = 0; i < maxCount; i++) {
			if(i == 0)
				playerSpawner.Spawn(gameController.GetSpawnPoint(), GetSpawnRotation(), container);
			else {
				enemySpawners[enemySpawnerIndex++ % enemySpawners.Length].Spawn(gameController.GetSpawnPoint(), GetSpawnRotation(), container);
			}
		}
	}

	public Vector3 GetRandomUnitPoint() {
		return units[UnityEngine.Random.Range(0, units.Count)].position;
	}

	private void AddUnit(Unit unit) {
		units.Add(unit);

		if(onUnitSpawned != null)
			onUnitSpawned(units.Count);
	}

	private void RemoveUnit(Unit unit, int id) {
		units.Remove(unit);

		if(onUnitRemoved != null)
			onUnitRemoved(units.Count, id);
	}

	private Quaternion GetSpawnRotation() {
		return Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360), Vector3.back);
	}
}
