﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusController : MonoBehaviour {
	public BonusSpawner[] bonusSpawners;
	public Vector2 spawnDelay;
	public float prewarmRate = .2f;

	private GameController gameController;
	private int maxCount;
	private Transform container;

	public List<Bonus> bonuses {
		get; private set;
	}

	private float randomSpawnDelay {
		get { return Random.Range(spawnDelay.x, spawnDelay.y); }
	}

	public void Setup(GameController gameController, int maxCount) {
		this.gameController = gameController;
		this.maxCount = maxCount;

		for(int i = 0; i < bonusSpawners.Length; i++) {
			bonusSpawners[i].onBonusRemoved += (Bonus bonus) => {
				bonuses.Remove(bonus);
			};
		}
	}

	public void Run(Transform container) {
		this.container = container;
		bonuses = new List<Bonus>();

		for(int i = 0; i < Mathf.RoundToInt(prewarmRate * maxCount); i++)
			Spawn();

		Stop();
		Invoke("SpawnBonus", randomSpawnDelay);
	}

	public void Stop() {
		CancelInvoke("SpawnBonus");
	}

	public Vector3 GetRandomBonusPoint() {
		return bonuses.Count > 0 ? bonuses[Random.Range(0, bonuses.Count)].position : Vector3.zero;
	}

	private void SpawnBonus() {
		if(bonuses.Count < maxCount) {
			Spawn();

			Invoke("SpawnBonus", randomSpawnDelay);
		}
	}

	private void Spawn() {
		var spawner = bonusSpawners[Random.Range(0, bonusSpawners.Length)];
		var bonus = spawner.Spawn(gameController.GetSpawnPoint(), container);

		bonuses.Add(bonus);
	}
}
