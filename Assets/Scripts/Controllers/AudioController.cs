﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour {
	public AudioSource jingleSource;
	public AudioSource gameMusicSource;

	public AudioClip startClip;
	public AudioClip winClip;
	public AudioClip loseClip;

	public AudioClip[] gameMusicClips;

	public AudioMixerSnapshot menuSnapshot;
	public AudioMixerSnapshot gameSnapshot;
	public AudioMixerSnapshot gameoverSnapshot;
	public AudioMixerSnapshot finishSnapshot;

	private int gameMusicClipIndex;

	public void PlayMenuMusic() {
		CancelInvoke("TransitionToFinish");
		menuSnapshot.TransitionTo(.5f);
	}

	public void PlayGameMusic() {
		gameMusicSource.Stop();
		gameMusicSource.clip = gameMusicClips[gameMusicClipIndex++ % gameMusicClips.Length];
		gameMusicSource.Play();

		gameSnapshot.TransitionTo(.5f);
	}

	public void PlayStartSound() {
		PlayJingle(startClip);

		Invoke("PlayGameMusic", startClip.length);
	}

	public void PlayWinSound() {
		PlayJingle(winClip);

		Invoke("TransitionToFinish", winClip.length);
	}

	public void PlayLoseSound() {
		PlayJingle(loseClip);

		Invoke("TransitionToFinish", loseClip.length);
	}

	private void PlayJingle(AudioClip clip) {
		gameoverSnapshot.TransitionTo(0f);

		jingleSource.PlayOneShot(clip);
	}

	private void TransitionToFinish() {
		finishSnapshot.TransitionTo(.5f);
	}
}
